import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<bool> _selections = List.generate(3, (index) => false);
  List<int> lwsv = List.generate(25, (index) => index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: ListWheelScrollView(
        children: lwsv.map((index) {
          return Container(
            height: 30,
            width: 150,
            color: Colors.purple,
          );
        }).toList(),
        itemExtent: 30,
      )
          /*Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              children: [
                Image.network('https://i.pinimg.com/originals/0a/4d/cb/0a4dcb92fa2d3c601b58d72720d6bec4.jpg'),
                Positioned.fill(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                    child: Container(
                      color: Colors.black.withOpacity(0.1),
                    ),
                  ),
                ),
              ],
            ),
            ShaderMask(
              shaderCallback: (someArg) => RadialGradient(
                center: Alignment.bottomRight,
                radius: 3.0,
                colors: [Colors.purple, Colors.pinkAccent],
                tileMode: TileMode.mirror,
              ).createShader(someArg),
              child: Text(
                "it's awesome",
                style: TextStyle(color: Colors.white, fontSize: 40.0),
              ),
            ),
            ToggleButtons(
              children: [
                Icon(Icons.store),
                Icon(Icons.margin),
                Icon(Icons.cake),
              ],
              isSelected: _selections,
              color: Colors.orange,
              selectedColor: Colors.blue,
              onPressed: (int ind) {
                setState(() {
                  _selections[ind] = !_selections[ind];
                });
              },
            ),*/
          /*  DraggableScrollableSheet(
              maxChildSize: 0.6,
                minChildSize: 0.3,
                builder: (context, scrollController){
              return SingleChildScrollView(
                controller: scrollController,

                child: Image.network('https://cutewallpaper.org/21/4k-wallpaper-for-mobile-1920x1080/Deadpool-2-Movie-Bullets-Poster-4K-Wallpaper-Best-Wallpapers.jpg'),
              );
            })*/
          // ],
          // ),
          ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
